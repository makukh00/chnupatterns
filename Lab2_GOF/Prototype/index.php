<?php

use Prototype\Trees\AppleTree;
use Prototype\Trees\PearTree;
use Prototype\Trees\TreeInterface;

require_once 'Trees/TreeInterface.php';
require_once 'Trees/AppleTree.php';
require_once 'Trees/PearTree.php';

// Створюємо оригінальне яблуневе дерево
$appleTree = new AppleTree();
clientCode($appleTree);

// Створюємо оригінальне грушеве дерево
$pearTree = new PearTree();
clientCode($pearTree);

function clientCode(TreeInterface $prototype) {
    $treeClone = $prototype->clone();
    echo "Клоновано дерево типу: " . $treeClone->getType() . "\n";
    echo $treeClone->grow() . "\n";
}