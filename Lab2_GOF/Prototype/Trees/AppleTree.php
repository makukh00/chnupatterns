<?php

namespace Prototype\Trees;

class AppleTree implements TreeInterface {
    public function getType(): string {
        return "Яблуня";
    }

    public function grow(): string {
        return "Яблуня росте та приносить соковиті яблука.";
    }

    public function clone(): TreeInterface {
        return new AppleTree();
    }
}