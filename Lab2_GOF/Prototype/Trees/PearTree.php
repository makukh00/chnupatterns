<?php

namespace Prototype\Trees;

class PearTree implements TreeInterface {
    public function getType(): string {
        return "Груша";
    }

    public function grow(): string {
        return "Груша росте та приносить солодкі груші.";
    }

    public function clone(): TreeInterface {
        return new PearTree();
    }
}