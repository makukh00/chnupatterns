<?php


use Factory_Method\Factories\AppleTreeFactory;
use Factory_Method\Factories\PearTreeFactory;
use Factory_Method\Factories\TreeFactory;

require_once 'Trees/TreeInterface.php';
require_once 'Trees/AppleTree.php';
require_once 'Trees/PearTree.php';
require_once 'Factories/TreeFactory.php';
require_once 'Factories/AppleTreeFactory.php';
require_once 'Factories/PearTreeFactory.php';

//створення яблуні
$appleFactory = new AppleTreeFactory();
clientCode($appleFactory);

//створення груші
$pearFactory = new PearTreeFactory();
clientCode($pearFactory);

function clientCode(TreeFactory $factory)
{
    $tree = $factory->getTree();
    echo $tree->grow() . "\n";
}