<?php

namespace Factory_Method\Factories;

use Factory_Method\Trees\AppleTree;
use Factory_Method\Trees\TreeInterface;

// Фабрика для створення яблуні
class AppleTreeFactory extends TreeFactory {
    public function createTree(): TreeInterface {
        return new AppleTree();
    }
}