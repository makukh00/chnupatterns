<?php

namespace Factory_Method\Factories;

use Factory_Method\Trees\PearTree;
use Factory_Method\Trees\TreeInterface;

class PearTreeFactory extends TreeFactory {
    public function createTree(): TreeInterface {
        return new PearTree();
    }
}