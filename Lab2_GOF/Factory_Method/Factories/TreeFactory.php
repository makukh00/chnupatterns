<?php
namespace Factory_Method\Factories;

use Factory_Method\Trees\TreeInterface;

abstract class TreeFactory
{
    // Абстрактний метод для створення дерева
    abstract public function createTree(): TreeInterface;

    public function getTree(): TreeInterface {
        $tree = $this->createTree();
        echo "Створюється дерево " . $tree->getType() . ".\n";
        return $tree;
    }
}