<?php

namespace Factory_Method\Trees;

// Клас для груші
class PearTree implements TreeInterface {
    public function getType(): string {
        return "Груша";
    }

    public function grow(): string {
        return "Груша росте та приносить солодкі груші.";
    }
}