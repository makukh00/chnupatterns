<?php

namespace Factory_Method\Trees;

// Клас для яблуні
class AppleTree implements TreeInterface {
    public function getType(): string {
        return "Яблуня";
    }

    public function grow(): string {
        return "Яблуня росте та приносить соковиті яблука.";
    }
}