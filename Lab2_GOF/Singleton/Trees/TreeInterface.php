<?php

namespace Singleton\Trees;

interface TreeInterface
{
    public function getType(): string; // Метод для отримання типу дерева
    public function grow(): string;    // Метод для опису росту дерева
}