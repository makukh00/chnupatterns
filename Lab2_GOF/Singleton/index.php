<?php

namespace Singleton;

require_once 'Trees/TreeInterface.php';
require_once 'Trees/AppleTree.php';
require_once 'Trees/PlumTree.php';
require_once 'TreeSingleton.php';

// Отримуємо єдиний екземпляр яблуні
$appleTreeSingleton = TreeSingleton::getAppleTreeInstance();
echo "Створено дерево типу: " . $appleTreeSingleton->getType() . "\n";
echo $appleTreeSingleton->grow() . "\n";

// Отримуємо єдиний екземпляр сливи
$plumTreeSingleton = TreeSingleton::getPlumTreeInstance();
echo "Створено дерево типу: " . $plumTreeSingleton->getType() . "\n";
echo $plumTreeSingleton->grow() . "\n";

// Спробуємо отримати той самий екземпляр яблуні
$anotherAppleTreeSingleton = TreeSingleton::getAppleTreeInstance();
if ($appleTreeSingleton === $anotherAppleTreeSingleton) {
    echo "Це той самий екземпляр яблуні.\n";
}

// Спробуємо отримати той самий екземпляр сливи
$anotherPlumTreeSingleton = TreeSingleton::getPlumTreeInstance();
if ($plumTreeSingleton === $anotherPlumTreeSingleton) {
    echo "Це той самий екземпляр сливи.\n";
}