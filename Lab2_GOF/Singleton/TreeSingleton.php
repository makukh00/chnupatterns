<?php

namespace Singleton;

use Singleton\Trees\AppleTree;
use Singleton\Trees\PlumTree;
use Singleton\Trees\TreeInterface;

class TreeSingleton
{
    private static ?TreeSingleton $appleTreeInstance = null;
    private static ?TreeSingleton $plumTreeInstance = null;
    private TreeInterface $tree;

    // Закритий конструктор для запобігання створення екземплярів ззовні
    private function __construct(TreeInterface $tree)
    {
        $this->tree = $tree;
    }

    // Метод для отримання єдиного екземпляра яблуні
    public static function getAppleTreeInstance(): TreeInterface
    {
        if (self::$appleTreeInstance === null) {
            self::$appleTreeInstance = new TreeSingleton(new AppleTree());
        }
        return self::$appleTreeInstance->tree;
    }

    // Метод для отримання єдиного екземпляра сливи
    public static function getPlumTreeInstance(): TreeInterface
    {
        if (self::$plumTreeInstance === null) {
            self::$plumTreeInstance = new TreeSingleton(new PlumTree());
        }
        return self::$plumTreeInstance->tree;
    }

    // Забороняємо клонування
    private function __clone()
    {
    }
}