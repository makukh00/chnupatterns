<?php

namespace Builder\Trees;

class PlumTree implements TreeInterface
{
    public function getType(): string {
        return "Слива";
    }

    public function grow(): string {
        return "Слива росте та приносить соковиті сливи.";
    }
}