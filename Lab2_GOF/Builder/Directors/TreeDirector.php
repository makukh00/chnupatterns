<?php

namespace Builder\Directors;

use Builder\Builders\TreeBuilderInterface;

// Директор для керування процесом створення дерев
class TreeDirector
{
    private TreeBuilderInterface $builder;

    public function setBuilder(TreeBuilderInterface $builder): void
    {
        $this->builder = $builder;
    }

    public function buildTree(): void
    {
        $this->builder->createTree();
    }
}