<?php

namespace Builder\Builders;

use Builder\Trees\PlumTree;
use Builder\Trees\TreeInterface;

class PlumTreeBuilder implements TreeBuilderInterface
{
    private TreeInterface $tree;

    public function createTree(): void
    {
        $this->tree = new PlumTree();
    }

    public function getTree(): TreeInterface
    {
        return $this->tree;
    }
}