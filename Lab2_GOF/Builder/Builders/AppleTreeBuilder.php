<?php

namespace Builder\Builders;

use Builder\Trees\AppleTree;
use Builder\Trees\TreeInterface;

class AppleTreeBuilder implements TreeBuilderInterface
{
    private TreeInterface $tree;

    public function createTree(): void
    {
        $this->tree = new AppleTree();
    }

    public function getTree(): TreeInterface
    {
        return $this->tree;
    }
}