<?php

namespace Builder\Builders;

use Builder\Trees\TreeInterface;

interface TreeBuilderInterface
{
    public function createTree(): void;  // Метод для створення дерева

    public function getTree(): TreeInterface;     // Метод для отримання дерева
}