<?php

namespace Builder;

use Builder\Builders\AppleTreeBuilder;
use Builder\Builders\PlumTreeBuilder;
use Builder\Directors\TreeDirector;

require_once 'Trees/TreeInterface.php';
require_once 'Trees/AppleTree.php';
require_once 'Trees/PlumTree.php';
require_once 'Builders/TreeBuilderInterface.php';
require_once 'Builders/AppleTreeBuilder.php';
require_once 'Builders/PlumTreeBuilder.php';
require_once 'Directors/TreeDirector.php';

// Функція для демонстрації роботи будівельника
function clientCode(TreeDirector $director) {
    $appleTreeBuilder = new AppleTreeBuilder();
    $director->setBuilder($appleTreeBuilder);

    $director->buildTree();
    $appleTree = $appleTreeBuilder->getTree();
    echo "Створено дерево типу: " . $appleTree->getType() . "\n";
    echo $appleTree->grow() . "\n";

    $plumTreeBuilder = new PlumTreeBuilder();
    $director->setBuilder($plumTreeBuilder);

    $director->buildTree();
    $plumTree = $plumTreeBuilder->getTree();
    echo "Створено дерево типу: " . $plumTree->getType() . "\n";
    echo $plumTree->grow() . "\n";
}

// Використання директора для створення дерев
$director = new TreeDirector();
clientCode($director);