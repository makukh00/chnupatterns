<?php

namespace Abstract_Factory;

use Abstract_Factory\Trees\TreeInterface;

abstract class AbstractTreeFactory
{
    abstract public function createAppleTree(): TreeInterface;

    abstract public function createPearTree(): TreeInterface;
}