<?php

namespace Abstract_Factory;

use Abstract_Factory\Trees\AppleTree;
use Abstract_Factory\Trees\PearTree;
use Abstract_Factory\Trees\TreeInterface;

class FruitTreeFactory extends AbstractTreeFactory
{
    public function createAppleTree(): TreeInterface
    {
        return new AppleTree();
    }

    public function createPearTree(): TreeInterface
    {
        return new PearTree();
    }
}