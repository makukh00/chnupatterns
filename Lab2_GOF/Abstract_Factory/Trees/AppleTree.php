<?php

namespace Abstract_Factory\Trees;

use Abstract_Factory\Trees\TreeInterface;

class AppleTree implements TreeInterface
{

    public function getType(): string
    {
        return "Яблуня";
    }

    public function grow(): string
    {
        return "Яблуня росте та приносить соковиті яблука.";
    }
}