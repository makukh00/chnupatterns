<?php

namespace Abstract_Factory\Trees;

use Abstract_Factory\Trees\TreeInterface;

class PearTree implements TreeInterface
{

    public function getType(): string
    {
        return "Груша";
    }

    public function grow(): string
    {
        return "Груша росте та приносить солодкі груші.";
    }
}