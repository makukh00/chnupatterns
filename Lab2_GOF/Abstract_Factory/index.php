<?php

namespace Abstract_Factory;
require_once 'AbstractTreeFactory.php';
require_once 'FruitTreeFactory.php';
require_once 'Trees/TreeInterface.php';
require_once 'Trees/AppleTree.php';
require_once 'Trees/PearTree.php';


function clientCode(AbstractTreeFactory $factory) {
    $appleTree = $factory->createAppleTree();
    $pearTree = $factory->createPearTree();

    echo "Створено дерево типу: " . $appleTree->getType() . "\n";
    echo $appleTree->grow() . "\n";

    echo "Створено дерево типу: " . $pearTree->getType() . "\n";
    echo $pearTree->grow() . "\n";
}

// Використання фабрики для створення дерев
$fruitTreeFactory = new FruitTreeFactory();
clientCode($fruitTreeFactory);