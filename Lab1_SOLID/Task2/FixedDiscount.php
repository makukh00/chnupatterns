<?php

namespace Task2;

/**
 * Open/Closed Principle
 * Реалізує DiscountInterface
 */
class FixedDiscount implements DiscountInterface
{
    private float $discountAmount;

    public function __construct(float $discountAmount)
    {
        $this->discountAmount = $discountAmount;
    }

    public function apply(float $price): float
    {
        $newPrice = $price - $this->discountAmount;
        if ($newPrice < 0) {
            throw new \InvalidArgumentException("Знижка перевищує ціну продукту");
        }
        return $newPrice;
    }
}