<?php

namespace Task2;

/**
 * Interface Segregation Principle
 * Інтерфейс для застосування знижки
 */
interface DiscountInterface
{
    public function apply(float $price): float;
}