<?php

namespace Task2;

/**
 *  Клас Product відповідає Single Responsibility Principle
 *  Призначений для зберіганням і маніпуляцією даними продукту
 */
class Product
{
    private string $name;
    private float $price;

    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        if ($price < 0) {
            throw new \InvalidArgumentException("Ціна не може бути негативною");
        }
        $this->price = $price;
    }

    // Використання Dependency Inversion Principle:
    // Метод приймає інтерфейс, а не конкретний клас
    public function applyDiscount(DiscountInterface $discount): void
    {
        $newPrice = $discount->apply($this->price);
        $this->setPrice($newPrice);
    }
}