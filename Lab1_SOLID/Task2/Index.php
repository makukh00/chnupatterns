<?php

namespace Task2;

use InvalidArgumentException;

require_once 'Product.php';
require_once 'DiscountInterface.php';
require_once 'FixedDiscount.php';

try {
    // Створення продукту
    $product = new Product("Молокія Йогурт", 100.0);
    echo "Продукт: " . $product->getName() . " - ціна: " . $product->getPrice() . " uah.\n";

    // Створення знижки
    $discount = new FixedDiscount(20.0);

    // Застосування знижки
    $product->applyDiscount($discount);

    echo "Новий ціна продукту: " . $product->getPrice();
} catch (InvalidArgumentException $e) {
    echo "Error: " . $e->getMessage();
}