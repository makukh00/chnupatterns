<?php

namespace Task3;

use DateTime;

/**
 * Open/Closed Principle
 * Клас Product реалізує інтерфейс ProductInterface і може бути розширений без модифікації існуючого коду
 */
class Product implements ProductInterface
{
    private string $name;
    private Unit $unit;
    private float $pricePerUnit;
    private int $quantity;
    private DateTime $lastRestocked;

    /**
     * @param string $name
     * @param Unit $unit
     * @param float $pricePerUnit
     * @param int $quantity
     * @param DateTime $lastRestocked
     */
    public function __construct(
        string   $name,
        Unit     $unit,
        float    $pricePerUnit,
        int      $quantity,
        DateTime $lastRestocked
    ) {
        $this->name = $name;
        $this->unit = $unit;
        $this->pricePerUnit = $pricePerUnit;
        $this->quantity = $quantity;
        $this->lastRestocked = $lastRestocked;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUnit(): Unit
    {
        return $this->unit;
    }

    public function getPricePerUnit(): float
    {
        return $this->pricePerUnit;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getLastRestocked(): DateTime
    {
        return $this->lastRestocked;
    }
}