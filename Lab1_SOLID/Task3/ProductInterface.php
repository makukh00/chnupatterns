<?php

namespace Task3;

/**
 * Interface Segregation Principle
 * Інтерфейс для опису товару
 */
interface ProductInterface
{
    public function getName(): string;
    public function getUnit(): Unit;
    public function getPricePerUnit(): float;
    public function getQuantity(): int;
    public function getLastRestocked(): \DateTime;
}