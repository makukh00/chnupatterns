<?php

namespace Task3;

/**
 * Single Responsibility Principle
 * Клас Unit відповідає тільки за зберігання одиниці виміру товару
 */
class Unit
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}