<?php

namespace Task3;

use DateTime;

/**
 * Single Responsibility Principle
 * Клас Warehouse відповідає тільки за зберігання і управління товарами на складі
 */
class Warehouse
{
    private array $products = [];

    // Додавання товару до складу
    public function addProduct(ProductInterface $product): void
    {
        $this->products[] = $product;
    }

    // Отримання всіх товарів
    public function getProducts(): array
    {
        return $this->products;
    }

    // Отримання товарів по назві
    public function getProductByName(string $name): ?ProductInterface
    {
        foreach ($this->products as $product) {
            if ($product->getName() === $name) {
                return $product;
            }
        }
        return null;
    }
}