<?php

namespace Task3;

use DateTime;
use Exception;

require_once 'Warehouse.php';
require_once 'ProductInterface.php';
require_once 'Product.php';
require_once 'Unit.php';

try {
    $warehouse = new Warehouse();

    $unit = new Unit("кг");
    $product1 = new Product("Яблука", $unit, 10.0, 85, new DateTime('2024-05-23'));
    $product2 = new Product("Апельсини", $unit, 12.0, 116, new DateTime('2024-05-21'));

    $warehouse->addProduct($product1);
    $warehouse->addProduct($product2);

    $products = $warehouse->getProducts();
    foreach ($products as $product) {
        echo "Назва: " . $product->getName() . ", Ціна: " . $product->getPricePerUnit()
            . " за " . $product->getUnit()->getName() . ", Кількість: " . $product->getQuantity()
            . ", Останній завіз: " . $product->getLastRestocked()->format('Y-m-d') . "\n";
    }
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
}