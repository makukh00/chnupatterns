<?php

namespace Task4\Reporting;

use Reporting\DocumentInterface;
use Task3\Warehouse;

/**
 * Single Responsibility Principle
 * Клас Reporting відповідає тільки за обробку звітності
 */
class Reporting
{
    private Warehouse $warehouse;

    public function __construct(Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
    }

    /**
     * Dependency Inversion Principle
     * Метод для створення документів, приймає інтерфейс DocumentInterface
     */
    public function createDocument(DocumentInterface $document): void
    {
        $document->generate();
    }

    /**
     * Звіт по інвентаризації (залишки на складі)
     */
    public function inventoryReport(): array
    {
        return $this->warehouse->getProducts();
    }
}