<?php

namespace Task4\Reporting;

use Reporting\DocumentInterface;

/**
 * Open/Closed Principle
 * Клас IncomingInvoice реалізує інтерфейс DocumentInterface і може бути розширений без зміни існуючого коду
 */
class IncomingInvoice implements DocumentInterface
{
    private string $details;

    public function __construct(string $details)
    {
        $this->details = $details;
    }

    public function generate(): void
    {
        echo "Генерація прибуткової накладної: " . $this->details . "\n";
    }
}