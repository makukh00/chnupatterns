<?php

namespace Reporting;

/**
 * Interface Segregation Principle
 * Інтерфейс для генерації документів
 */
interface DocumentInterface
{
    public function generate(): void;
}