<?php
namespace Task4;

require_once __DIR__ . '/../Task3/Warehouse.php';
require_once __DIR__ . '/../Task3/ProductInterface.php';
require_once __DIR__ . '/../Task3/Product.php';
require_once __DIR__ . '/../Task3/Unit.php';
require_once 'Reporting/Reporting.php';
require_once 'Reporting/DocumentInterface.php';
require_once 'Reporting/IncomingInvoice.php';
require_once 'Reporting/OutgoingInvoice.php';

use DateTime;
use Exception;
use Task3\Warehouse;
use Task3\Product;
use Task3\Unit;
use Task4\Reporting\Reporting;
use Task4\Reporting\IncomingInvoice;
use Task4\Reporting\OutgoingInvoice;

try {
    $warehouse = new Warehouse();

    $unit = new Unit("кг");
    $product1 = new Product("Яблука", $unit, 10.0, 85, new DateTime('2024-05-23'));
    $product2 = new Product("Апельсини", $unit, 12.0, 116, new DateTime('2024-05-21'));

    $warehouse->addProduct($product1);
    $warehouse->addProduct($product2);
    // створюємо об'єкт репорт та в якость параметру в конструктор передаємо продукти із складу
    $reporting = new Reporting($warehouse);

    $incomingInvoice = new IncomingInvoice("Яблука, 100 кг, 2024-05-23");
    $outgoingInvoice = new OutgoingInvoice("Апельсини, 50 кг, 2024-05-24");

    $reporting->createDocument($incomingInvoice);
    $reporting->createDocument($outgoingInvoice);

    $inventoryReport = $reporting->inventoryReport();
    foreach ($inventoryReport as $product) {
        echo "Назва: " . $product->getName() . ", Ціна: " . $product->getPricePerUnit() . " за " . $product->getUnit()->getName() . ", Кількість: " . $product->getQuantity() . ", Останній завіз: " . $product->getLastRestocked()->format('Y-m-d') . "\n";
    }
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
}