<?php

class Money
{
    private int $wholePart;
    private int $fractionalPart;
    private string $currency;

    public function __construct(
        int    $wholePart,
        int    $fractionalPart,
        string $currency
    )
    {
        $this->setWholePart($wholePart);
        $this->setFractionalPart($fractionalPart);
        $this->currency = $currency;
    }

    public function getWholePart(): int
    {
        return $this->wholePart;
    }

    public function getFractionalPart(): int
    {
        return $this->fractionalPart;
    }

    public function setWholePart(int $wholePart): void
    {
        if ($wholePart < 0) {
            throw new \InvalidArgumentException("Ціла частина не може бути негативною");
        }
        $this->wholePart = $wholePart;
    }

    public function setFractionalPart(int $fractionalPart): void
    {
        if ($fractionalPart < 0 || $fractionalPart >= 100) {
            throw new \InvalidArgumentException("Копійки повинні бути в діапазоні від 0 до 99");
        }
        $this->fractionalPart = $fractionalPart;
    }

    public function display(): void
    {
        echo sprintf("%d.%02d %s", $this->wholePart, $this->fractionalPart, $this->currency);
    }
}