<?php

require_once 'Money.php';
require_once 'MoneyDisplayInterface.php';
require_once 'MoneyDisplay.php';

try {
    $money = new Money(567, 89, 'UAH');
    $display = new MoneyDisplay();
    $display->display($money);
} catch (InvalidArgumentException $e) {
    echo "Error: " . $e->getMessage();
}