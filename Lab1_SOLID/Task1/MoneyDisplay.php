<?php

/**
 *  Клас, що виводить суму на екран, реалізловуючи інтерфейс
 */
class MoneyDisplay implements MoneyDisplayInterface
{
    public function display(Money $money): void
    {
        $money->display();
    }
}