<?php

/**
 * Dependency Inversion Principle
 */
interface MoneyDisplayInterface
{
    public function display(Money $money): void;
}