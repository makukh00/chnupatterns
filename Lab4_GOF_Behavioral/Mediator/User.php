<?php

namespace Mediator;

abstract class User
{
    protected ChatMediatorInterface $mediator;
    protected string $name;

    public function __construct(ChatMediatorInterface $mediator, string $name)
    {
        $this->mediator = $mediator;
        $this->name = $name;
    }

    abstract public function send(string $message): void;

    abstract public function receive(string $message): void;
}