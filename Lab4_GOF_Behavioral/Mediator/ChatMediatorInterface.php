<?php

namespace Mediator;

interface ChatMediatorInterface
{
    public function sendMessage(string $message, User $user): void;

    public function addUser(User $user): void;
}