<?php

namespace Mediator;

class ChatMediator implements ChatMediatorInterface
{
    private array $users = [];

    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    public function sendMessage(string $message, User $user): void
    {
        foreach ($this->users as $u) {
            if ($u !== $user) {
                $u->receive($message);
            }
        }
    }
}