<?php

namespace Mediator;

class ChatUser extends User
{
    public function send(string $message): void
    {
        echo $this->name . " sends: " . $message . "\n";
        $this->mediator->sendMessage($message, $this);
    }

    public function receive(string $message): void
    {
        echo $this->name . " receives: " . $message . "\n";
    }
}