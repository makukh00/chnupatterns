<?php

namespace Mediator;


require_once 'ChatMediatorInterface.php';
require_once 'ChatMediator.php';
require_once 'User.php';
require_once 'ChatUser.php';

// Використання посередника для керування чатом
$mediator = new ChatMediator();

$user1 = new ChatUser($mediator, "Alice");
$user2 = new ChatUser($mediator, "Bob");
$user3 = new ChatUser($mediator, "Charlie");

$mediator->addUser($user1);
$mediator->addUser($user2);
$mediator->addUser($user3);

$user1->send("Hello, everyone!");
$user2->send("Hi, Alice!");

