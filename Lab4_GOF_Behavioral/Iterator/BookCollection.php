<?php

namespace Iterator;

require_once 'IterableInterface.php';
require_once 'BookIterator.php';

class BookCollection implements IterableInterface
{
    private array $books = [];

    public function addBook(Book $book): void
    {
        $this->books[] = $book;
    }

    public function getIterator(): IteratorInterface
    {
        return new BookIterator($this->books);
    }
}