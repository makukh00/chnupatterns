<?php

namespace Iterator;

require_once 'IteratorInterface.php';

class BookIterator implements IteratorInterface
{
    private array $books;
    private int $index = 0;

    public function __construct(array $books)
    {
        $this->books = $books;
    }

    public function hasNext(): bool
    {
        return isset($this->books[$this->index]);
    }

    public function next(): Book
    {
        return $this->books[$this->index++];
    }
}