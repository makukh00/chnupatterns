<?php
namespace Iterator;

require_once 'Book.php';
require_once 'BookCollection.php';
require_once 'BookIterator.php';
require_once 'IteratorInterface.php';
require_once 'IterableInterface.php';

// Використання ітератора для обходу колекції книг
$book1 = new Book("1984", "George Orwell");
$book2 = new Book("To Kill a Mockingbird", "Harper Lee");
$book3 = new Book("The Great Gatsby", "F. Scott Fitzgerald");

$collection = new BookCollection();
$collection->addBook($book1);
$collection->addBook($book2);
$collection->addBook($book3);

$iterator = $collection->getIterator();
while ($iterator->hasNext()) {
    $book = $iterator->next();
    echo $book->getTitle() . " by " . $book->getAuthor() . "\n";
}