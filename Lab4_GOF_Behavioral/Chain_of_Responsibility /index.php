<?php

namespace Chain_of_responsibility;

require_once 'HandlerInterface.php';
require_once 'AbstractHandler.php';
require_once 'Level1Support.php';
require_once 'Level2Support.php';
require_once 'Level3Support.php';

// Встановлюємо ланцюг обробників
$l1 = new Level1Support();
$l2 = new Level2Support();
$l3 = new Level3Support();

$l1->setNext($l2)->setNext($l3);

// Використання ланцюга для обробки запиту
function clientCode(HandlerInterface $handler, string $request)
{
    if ($handler->handle($request)) {
        echo "Запит \"$request\" був оброблений.\n";
    } else {
        echo "Запит \"$request\" не може бути оброблений.\n";
    }
}

clientCode($l1, "Reset password");
clientCode($l1, "Fix server issue");
clientCode($l1, "Install software");

