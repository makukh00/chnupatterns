<?php

namespace Chain_of_responsibility;

interface HandlerInterface
{
    public function setNext(HandlerInterface $handler): HandlerInterface;

    public function handle(string $request): bool;
}