<?php

namespace Chain_of_responsibility;

class Level2Support extends AbstractHandler
{
    public function handle(string $request): bool
    {
        if ($request === "Fix server issue") {
            echo "Level 2 Support: Handling request - $request\n";
            return true;
        }
        return parent::handle($request);
    }
}