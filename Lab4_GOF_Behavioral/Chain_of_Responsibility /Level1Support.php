<?php

namespace Chain_of_responsibility;

class Level1Support extends AbstractHandler
{
    public function handle(string $request): bool
    {
        if ($request === "Reset password") {
            echo "Level 1 Support: Handling request - $request\n";
            return true;
        }
        return parent::handle($request);
    }
}