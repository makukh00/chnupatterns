<?php

namespace Chain_of_responsibility;

class Level3Support extends AbstractHandler
{
    public function handle(string $request): bool
    {
        if ($request === "Install software") {
            echo "Level 3 Support: Handling request - $request\n";
            return true;
        }
        return parent::handle($request);
    }
}