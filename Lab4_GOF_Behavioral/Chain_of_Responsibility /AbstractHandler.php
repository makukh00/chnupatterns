<?php

namespace Chain_of_responsibility;

abstract class AbstractHandler implements HandlerInterface {
    private ?HandlerInterface $nextHandler = null;

    public function setNext(HandlerInterface $handler): HandlerInterface {
        $this->nextHandler = $handler;
        return $handler;
    }

    public function handle(string $request): bool {
        if ($this->nextHandler) {
            return $this->nextHandler->handle($request);
        }

        return false;
    }
}