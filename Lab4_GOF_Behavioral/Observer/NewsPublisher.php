<?php

namespace Observer;

class NewsPublisher implements SubjectInterface
{
    private array $observers = [];
    private string $news = "";

    public function attach(ObserverInterface $observer): void
    {
        $this->observers[] = $observer;
    }

    public function detach(ObserverInterface $observer): void
    {
        $this->observers = array_filter(
            $this->observers,
            function ($o) use ($observer) {
                return $o !== $observer;
            }
        );
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this->news);
        }
    }

    public function addNews(string $news): void
    {
        $this->news = $news;
        $this->notify();
    }
}