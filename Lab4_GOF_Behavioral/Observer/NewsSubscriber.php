<?php

namespace Observer;

class NewsSubscriber implements ObserverInterface
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function update(string $news): void
    {
        echo $this->name . " received news: " . $news . "\n";
    }
}