<?php

namespace Observer;


require_once 'SubjectInterface.php';
require_once 'ObserverInterface.php';
require_once 'NewsPublisher.php';
require_once 'NewsSubscriber.php';

// Використання патерну спостерігач для підписки на новини
$newsPublisher = new NewsPublisher();

$subscriber1 = new NewsSubscriber("Alice");
$subscriber2 = new NewsSubscriber("Bob");

$newsPublisher->attach($subscriber1);
$newsPublisher->attach($subscriber2);

$newsPublisher->addNews("New PHP 8.4 features released!");
$newsPublisher->addNews("New design patterns in PHP!");