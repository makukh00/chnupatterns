<?php

namespace Memento;

class TextEditor
{
    private string $content = "";

    public function write(string $text): void
    {
        $this->content .= $text;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function save(): TextEditorMemento
    {
        return new TextEditorMemento($this->content);
    }

    public function restore(TextEditorMemento $memento): void
    {
        $this->content = $memento->getContent();
    }
}