<?php

namespace Memento;

class Caretaker
{
    private TextEditor $editor;
    private array $mementos = [];

    public function __construct(TextEditor $editor)
    {
        $this->editor = $editor;
    }

    public function save(): void
    {
        $this->mementos[] = $this->editor->save();
    }

    public function undo(): void
    {
        if (empty($this->mementos)) {
            return;
        }

        $memento = array_pop($this->mementos);
        $this->editor->restore($memento);
    }
}