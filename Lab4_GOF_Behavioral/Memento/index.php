<?php

namespace Memento;


require_once 'TextEditor.php';
require_once 'TextEditorMemento.php';
require_once 'Caretaker.php';

// Використання мементо для збереження та відновлення стану текстового редактора
$editor = new TextEditor();
$caretaker = new Caretaker($editor);

$editor->write("Hello, ");
$caretaker->save();

$editor->write("World!");
$caretaker->save();

$editor->write(" How are you?");
echo "Current content: " . $editor->getContent() . "\n";

$caretaker->undo();
echo "After undo: " . $editor->getContent() . "\n";

$caretaker->undo();
echo "After second undo: " . $editor->getContent() . "\n";



