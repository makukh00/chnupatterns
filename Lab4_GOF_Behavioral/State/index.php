<?php

namespace State;


require_once 'Door.php';
require_once 'StateInterface.php';
require_once 'OpenState.php';
require_once 'ClosedState.php';

// Використання патерну стан для керування станом дверей
$door = new Door(new ClosedState());

echo $door->open() . "\n";
echo $door->close() . "\n";
echo $door->open() . "\n";
echo $door->close() . "\n";

