<?php

namespace State;

class OpenState implements StateInterface
{
    public function open(Door $door): string
    {
        return "The door is already open.";
    }

    public function close(Door $door): string
    {
        $door->setState(new ClosedState());
        return "The door is now closed.";
    }
}