<?php

namespace State;

class Door
{
    private StateInterface $state;

    public function __construct(StateInterface $state)
    {
        $this->state = $state;
    }

    public function setState(StateInterface $state): void
    {
        $this->state = $state;
    }

    public function open(): string
    {
        return $this->state->open($this);
    }

    public function close(): string
    {
        return $this->state->close($this);
    }
}