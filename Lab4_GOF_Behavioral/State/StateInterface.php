<?php

namespace State;

interface StateInterface
{
    public function open(Door $door): string;

    public function close(Door $door): string;
}