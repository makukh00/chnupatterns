<?php

namespace State;

class ClosedState implements StateInterface
{
    public function open(Door $door): string
    {
        $door->setState(new OpenState());
        return "The door is now open.";
    }

    public function close(Door $door): string
    {
        return "The door is already closed.";
    }
}