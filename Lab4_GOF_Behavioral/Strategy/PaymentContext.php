<?php

namespace Strategy;


class PaymentContext
{
    private PaymentStrategyInterface $strategy;

    public function __construct(PaymentStrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function setStrategy(PaymentStrategyInterface $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function pay(int $amount): void
    {
        $this->strategy->pay($amount);
    }
}