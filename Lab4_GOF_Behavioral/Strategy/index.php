<?php

namespace Strategy;

require_once 'PaymentStrategyInterface.php';
require_once 'CreditCardPayment.php';
require_once 'PayPalPayment.php';
require_once 'PaymentContext.php';

// Використання стратегії для обробки платежів
$paymentContext = new PaymentContext(new CreditCardPayment());
$paymentContext->pay(100);

$paymentContext->setStrategy(new PayPalPayment());
$paymentContext->pay(200);

