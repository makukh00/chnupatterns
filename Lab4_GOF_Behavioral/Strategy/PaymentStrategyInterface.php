<?php

namespace Strategy;

interface PaymentStrategyInterface
{
    public function pay(int $amount): void;

}