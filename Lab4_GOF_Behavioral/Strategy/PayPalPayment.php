<?php

namespace Strategy;

class PayPalPayment implements PaymentStrategyInterface
{
    public function pay(int $amount): void
    {
        echo "Paid $amount using PayPal.\n";
    }
}