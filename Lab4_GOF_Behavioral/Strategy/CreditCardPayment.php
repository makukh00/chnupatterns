<?php

namespace Strategy;

class CreditCardPayment implements PaymentStrategyInterface
{
    public function pay(int $amount): void
    {
        echo "Paid $amount using Credit Card.\n";
    }
}