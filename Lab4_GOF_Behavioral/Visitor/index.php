<?php

namespace Visitor;


require_once 'ItemInterface.php';
require_once 'Book.php';
require_once 'Fruit.php';
require_once 'ShoppingCartVisitorInterface.php';
require_once 'ShoppingCartVisitor.php';
require_once 'ShoppingCart.php';

// Використання відвідувача для обчислення вартості товарів у кошику
$items = [
    new Book("1234", 20),
    new Book("5678", 100),
    new Fruit("Apple", 2, 3),
    new Fruit("Banana", 1, 2)
];

$cart = new ShoppingCart();
$total = $cart->calculateTotal($items);
echo "Total cost: $total\n";

