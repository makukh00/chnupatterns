<?php

namespace Visitor;

class Fruit implements ItemInterface
{
    private string $name;
    private int $pricePerKg;
    private int $weight;

    public function __construct(string $name, int $pricePerKg, int $weight)
    {
        $this->name = $name;
        $this->pricePerKg = $pricePerKg;
        $this->weight = $weight;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPricePerKg(): int
    {
        return $this->pricePerKg;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function accept(ShoppingCartVisitorInterface $visitor): int
    {
        return $visitor->visitFruit($this);
    }
}