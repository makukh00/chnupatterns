<?php

namespace Visitor;

class Book implements ItemInterface
{
    private string $isbn;
    private int $price;

    public function __construct(string $isbn, int $price)
    {
        $this->isbn = $isbn;
        $this->price = $price;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function accept(ShoppingCartVisitorInterface $visitor): int
    {
        return $visitor->visitBook($this);
    }
}