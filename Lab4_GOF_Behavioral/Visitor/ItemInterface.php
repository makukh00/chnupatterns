<?php

namespace Visitor;

interface ItemInterface
{
    public function accept(ShoppingCartVisitorInterface $visitor): int;
}