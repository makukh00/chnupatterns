<?php

namespace Visitor;


class ShoppingCartVisitor implements ShoppingCartVisitorInterface
{
    public function visitBook(Book $book): int
    {
        return $book->getPrice();
    }

    public function visitFruit(Fruit $fruit): int
    {
        return $fruit->getPricePerKg() * $fruit->getWeight();
    }
}