<?php

namespace Visitor;

interface ShoppingCartVisitorInterface
{
    public function visitBook(Book $book): int;

    public function visitFruit(Fruit $fruit): int;
}