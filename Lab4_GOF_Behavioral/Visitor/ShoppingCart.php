<?php

namespace Visitor;

class ShoppingCart
{
    public function calculateTotal(array $items): int
    {
        $visitor = new ShoppingCartVisitor();
        $sum = 0;
        foreach ($items as $item) {
            $sum += $item->accept($visitor);
        }
        return $sum;
    }
}