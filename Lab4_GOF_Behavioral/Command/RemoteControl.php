<?php

namespace Command;

class RemoteControl {
    private CommandInterface $command;

    public function setCommand(CommandInterface $command): void {
        $this->command = $command;
    }

    public function pressButton(): void {
        $this->command->execute();
    }
}