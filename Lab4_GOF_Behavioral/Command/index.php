<?php

namespace Command;

require_once 'CommandInterface.php';
require_once 'Light.php';
require_once 'LightOnCommand.php';
require_once 'LightOffCommand.php';
require_once 'RemoteControl.php';

// Використання команди для керування світлом
$light = new Light();
$lightOn = new LightOnCommand($light);
$lightOff = new LightOffCommand($light);

$remote = new RemoteControl();
$remote->setCommand($lightOn);
$remote->pressButton();

$remote->setCommand($lightOff);
$remote->pressButton();