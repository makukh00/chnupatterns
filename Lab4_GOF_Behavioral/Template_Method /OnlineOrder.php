<?php

namespace Template_Method;

class OnlineOrder extends OrderProcessor
{
    protected function selectItem(): void
    {
        echo "Item selected from online store.\n";
    }

    protected function processPayment(): void
    {
        echo "Payment processed through online payment gateway.\n";
    }

    protected function deliver(): void
    {
        echo "Item delivered through courier.\n";
    }
}