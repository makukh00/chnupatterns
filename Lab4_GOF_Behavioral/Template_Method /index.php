<?php

namespace Template_Method;

require_once 'OrderProcessor.php';
require_once 'OnlineOrder.php';
require_once 'StoreOrder.php';

// Використання шаблонного методу для обробки замовлень
$onlineOrder = new OnlineOrder();
$onlineOrder->processOrder();

echo "<===================================================> \n";
$storeOrder = new StoreOrder();
$storeOrder->processOrder();

