<?php

namespace Template_Method;

class StoreOrder extends OrderProcessor
{
    protected function selectItem(): void
    {
        echo "Item selected from store shelf.\n";
    }

    protected function processPayment(): void
    {
        echo "Payment processed at store counter.\n";
    }

    protected function deliver(): void
    {
        echo "Item handed to customer.\n";
    }
}