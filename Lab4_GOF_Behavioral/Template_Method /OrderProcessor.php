<?php

namespace Template_Method;

abstract class OrderProcessor
{
    public function processOrder(): void
    {
        $this->selectItem();
        $this->processPayment();
        $this->deliver();
    }

    abstract protected function selectItem(): void;

    abstract protected function processPayment(): void;

    abstract protected function deliver(): void;
}