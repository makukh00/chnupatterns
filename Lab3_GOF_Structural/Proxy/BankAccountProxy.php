<?php

namespace Proxy;

use Proxy\BankAccountInterface;

class BankAccountProxy implements BankAccountInterface
{
    private RealBankAccount $realAccount;

    public function __construct(RealBankAccount $realAccount)
    {
        $this->realAccount = $realAccount;
    }

    public function deposit(float $amount): void
    {
        $this->realAccount->deposit($amount);
    }

    public function withdraw(float $amount): void
    {
        $this->realAccount->withdraw($amount);
    }

    public function getBalance(): float
    {
        return $this->realAccount->getBalance();
    }
}