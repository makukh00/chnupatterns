<?php

namespace Proxy;

use Proxy\BankAccountInterface;

class RealBankAccount implements BankAccountInterface
{
    private float $balance = 0;

    public function deposit(float $amount): void
    {
        $this->balance += $amount;
    }

    public function withdraw(float $amount): void
    {
        if ($this->balance >= $amount) {
            $this->balance -= $amount;
        } else {
            echo "Недостатньо коштів\n";
        }
    }

    public function getBalance(): float
    {
        return $this->balance;
    }
}