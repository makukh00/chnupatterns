<?php
namespace Proxy;

require_once 'BankAccountInterface.php';
require_once 'RealBankAccount.php';
require_once 'BankAccountProxy.php';

// Використання проксі для доступу до банківського рахунку
function clientCode(BankAccountInterface $account)
{
    echo "Depositing 100...\n";
    $account->deposit(100);
    echo "Withdrawing 50...\n";
    $account->withdraw(50);
    echo "Balance: " . $account->getBalance() . "\n";
}

$realAccount = new RealBankAccount();
$proxy = new BankAccountProxy($realAccount);

clientCode($proxy);
