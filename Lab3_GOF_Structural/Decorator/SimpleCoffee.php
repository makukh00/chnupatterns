<?php

namespace Decorator;

class SimpleCoffee implements CoffeeInterface
{
    public function getCost(): float
    {
        return 30.0;
    }

    public function getDescription(): string
    {
        return "Simple coffee";
    }
}