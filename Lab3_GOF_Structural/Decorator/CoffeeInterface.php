<?php

namespace Decorator;

interface CoffeeInterface
{
    public function getCost(): float;

    public function getDescription(): string;
}