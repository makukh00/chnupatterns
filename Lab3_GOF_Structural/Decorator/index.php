<?php

namespace Decorator;

require_once 'CoffeeInterface.php';
require_once 'SimpleCoffee.php';
require_once 'CoffeeDecorator.php';
require_once 'MilkDecorator.php';
require_once 'SugarDecorator.php';

// Декоратор для додавання інгредієнтів до кави
function clientCode(CoffeeInterface $coffee)
{
    echo $coffee->getDescription() . ": $" . $coffee->getCost() . "\n";
}

$simpleCoffee = new SimpleCoffee();
clientCode($simpleCoffee);

$milkCoffee = new MilkDecorator($simpleCoffee);
clientCode($milkCoffee);

$sugarMilkCoffee = new SugarDecorator($milkCoffee);
clientCode($sugarMilkCoffee);