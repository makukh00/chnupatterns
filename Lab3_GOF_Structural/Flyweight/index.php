<?php
namespace Flyweight;

require_once 'TreeInterface.php';
require_once 'Tree.php';
require_once 'TreeFactory.php';

// Використання фабрики для створення та відображення дерев у лісі
function clientCode(TreeFactory $factory) {
    $tree1 = $factory->getTree("Apple", "Green");
    $tree2 = $factory->getTree("Plum", "Green");
    $tree3 = $factory->getTree("Poplar", "Yellow");

    echo $tree1->display(10, 20) . "\n";
    echo $tree2->display(15, 25) . "\n";
    echo $tree3->display(30, 40) . "\n";
}

$factory = new TreeFactory();
clientCode($factory);