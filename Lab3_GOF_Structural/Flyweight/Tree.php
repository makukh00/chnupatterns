<?php

namespace Flyweight;


class Tree implements TreeInterface {
    private string $type;
    private string $color;

    public function __construct(string $type, string $color) {
        $this->type = $type;
        $this->color = $color;
    }

    public function display(int $x, int $y): string {
        return "Tree of type $this->type with color $this->color is displayed at ($x, $y)";
    }
}