<?php

namespace Flyweight;


interface TreeInterface
{
    public function display(int $x, int $y): string;

}