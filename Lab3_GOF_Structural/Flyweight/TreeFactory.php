<?php

namespace Flyweight;

class TreeFactory {
    private array $trees = [];

    public function getTree(string $type, string $color): Tree {
        $key = $this->getKey($type, $color);
        if (!isset($this->trees[$key])) {
            $this->trees[$key] = new Tree($type, $color);
        }
        return $this->trees[$key];
    }

    private function getKey(string $type, string $color): string {
        return md5($type . $color);
    }
}