<?php

namespace Bridge;
interface ShapeInterface
{
    public function draw(): string;
}