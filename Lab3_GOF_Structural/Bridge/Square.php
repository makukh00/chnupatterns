<?php

namespace Bridge;

class Square implements ShapeInterface
{
    private ColorInterface $color;

    public function __construct(ColorInterface $color)
    {
        $this->color = $color;
    }

    public function draw(): string
    {
        return "Drawing a square in " . $this->color->fill();
    }
}
