<?php

namespace Bridge;

class RedColor implements ColorInterface
{
    public function fill(): string
    {
        return "red";
    }
}