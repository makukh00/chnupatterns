<?php

namespace Bridge;

interface ColorInterface
{
    public function fill(): string;
}