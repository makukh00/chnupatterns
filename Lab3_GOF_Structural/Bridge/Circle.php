<?php
namespace Bridge;

use Bridge\ShapeInterface;

class Circle implements ShapeInterface {
    private ColorInterface $color;

    public function __construct(ColorInterface $color) {
        $this->color = $color;
    }

    public function draw(): string {
        return "Drawing a circle in " . $this->color->fill();
    }
}