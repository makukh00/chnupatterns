<?php

namespace Bridge;

class BlueColor implements ColorInterface
{
    public function fill(): string
    {
        return "blue";
    }
}