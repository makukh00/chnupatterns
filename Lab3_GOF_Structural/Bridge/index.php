<?php

use Bridge\BlueColor;
use Bridge\Circle;
use Bridge\RedColor;
use Bridge\ShapeInterface;
use Bridge\Square;

require_once 'ShapeInterface.php';
require_once 'Circle.php';
require_once 'Square.php';
require_once 'ColorInterface.php';
require_once 'RedColor.php';
require_once 'BlueColor.php';

function clientCode(ShapeInterface $shape)
{
    echo $shape->draw() . "\n";
}

$red = new RedColor();
$blue = new BlueColor();

$redCircle = new Circle($red);
$blueSquare = new Square($blue);

clientCode($redCircle);
clientCode($blueSquare);
