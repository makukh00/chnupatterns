<?php

namespace Facade;

class Projector
{
    public function on(): void
    {
        echo "Projector is on\n";
    }

    public function off(): void
    {
        echo "Projector is off\n";
    }
}