<?php

namespace Facade;

class SoundSystem
{
    public function on(): void
    {
        echo "Sound system is on\n";
    }

    public function off(): void
    {
        echo "Sound system is off\n";
    }

    public function setVolume(int $level): void
    {
        echo "Setting sound system volume to $level\n";
    }
}