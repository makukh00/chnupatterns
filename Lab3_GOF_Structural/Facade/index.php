<?php

namespace Facade;

require_once 'HomeTheaterFacade.php';
require_once 'DVDPlayer.php';
require_once 'Projector.php';
require_once 'SoundSystem.php';

// Використання фасаду для керування домашньою системою розваг
$dvdPlayer = new DVDPlayer();
$projector = new Projector();
$soundSystem = new SoundSystem();

$homeTheater = new HomeTheaterFacade($dvdPlayer, $projector, $soundSystem);
$homeTheater->watchMovie("Avatar 2");
$homeTheater->endMovie();