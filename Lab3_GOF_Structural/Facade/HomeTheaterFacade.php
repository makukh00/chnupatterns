<?php

namespace Facade;

class HomeTheaterFacade
{
    /**
     * @var DVDPlayer
     */
    private $dvdPlayer;
    /**
     * @var Projector
     */
    private $projector;
    /**
     * @var SoundSystem
     */
    private $soundSystem;

    public function __construct(DVDPlayer $dvdPlayer, Projector $projector, SoundSystem $soundSystem)
    {
        $this->dvdPlayer = $dvdPlayer;
        $this->projector = $projector;
        $this->soundSystem = $soundSystem;
    }

    public function watchMovie(string $movie): void
    {
        echo "Getting ready to watch a movie...\n";
        $this->projector->on();
        $this->soundSystem->on();
        $this->soundSystem->setVolume(20);
        $this->dvdPlayer->on();
        $this->dvdPlayer->play($movie);
    }

    public function endMovie(): void
    {
        echo "Shutting down movie theater...\n";
        $this->dvdPlayer->off();
        $this->soundSystem->off();
        $this->projector->off();
    }
}