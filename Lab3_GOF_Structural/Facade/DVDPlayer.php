<?php

namespace Facade;

class DVDPlayer
{
    public function on(): void
    {
        echo "DVD Player is on\n";
    }

    public function off(): void
    {
        echo "DVD Player is off\n";
    }

    public function play(string $movie): void
    {
        echo "Playing movie: $movie\n";
    }
}