<?php

namespace Adapter;

class MP4Player implements VideoPlayerInterface {
    public function play(string $filename): string {
        return "Playing MP4 file: $filename";
    }
}