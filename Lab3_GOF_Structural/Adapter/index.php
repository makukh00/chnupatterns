<?php

use Adapter\MKVPlayer;
use Adapter\MP4Player;
use Adapter\VideoAdapter;
use Adapter\VideoPlayerInterface;

require_once 'VideoPlayerInterface.php';
require_once 'MP4Player.php';
require_once 'MKVPlayer.php';
require_once 'VideoAdapter.php';

// Використання адаптера для відтворення MKV файлів через MP4 плеєр
function clientCode(VideoPlayerInterface $player) {
    echo $player->play("example.mkv") . "\n";
}

$mp4Player = new MP4Player();
$mkvPlayer = new MKVPlayer();

$adapter = new VideoAdapter($mkvPlayer);
clientCode($adapter);