<?php

namespace Adapter;

class MKVPlayer
{
    public function playMKV(string $filename): string {
        return "Playing MKV file: $filename";
    }
}