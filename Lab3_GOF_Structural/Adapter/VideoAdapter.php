<?php

namespace Adapter;

use Adapter\VideoPlayerInterface;

class VideoAdapter implements VideoPlayerInterface
{
    private MKVPlayer $mkvPlayer;

    public function __construct(MKVPlayer $mkvPlayer) {
        $this->mkvPlayer = $mkvPlayer;
    }
    public function play(string $filename): string
    {
        return $this->mkvPlayer->playMKV($filename);
    }
}