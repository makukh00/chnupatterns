<?php

namespace Adapter;

interface VideoPlayerInterface
{
    public function play(string $filename): string;
}