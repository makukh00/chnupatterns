<?php

namespace Composite;

require_once 'EmployeeInterface.php';
require_once 'Employee.php';
require_once 'Manager.php';
require_once 'Department.php';

// Використання патерну композит для відображення структури компанії
function clientCode(EmployeeInterface $employee)
{
    echo $employee->getDetails() . "\n";
}

$developer1 = new Employee("Developer 1", "Software Engineer");
$developer2 = new Employee("Developer 2", "Software Engineer");

$manager = new Manager("Manager", "Development Manager");
$manager->add($developer1);
$manager->add($developer2);

$department = new Department("Development Department");
$department->add($manager);

clientCode($department);