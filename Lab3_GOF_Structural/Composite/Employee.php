<?php

namespace Composite;

class Employee implements EmployeeInterface
{
    private string $name;
    private string $position;

    public function __construct(string $name, string $position)
    {
        $this->name = $name;
        $this->position = $position;
    }

    public function getDetails(): string
    {
        return $this->position . ": " . $this->name;
    }
}