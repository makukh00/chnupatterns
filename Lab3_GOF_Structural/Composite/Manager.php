<?php

namespace Composite;

class Manager implements EmployeeInterface
{
    private string $name;
    private string $position;
    private array $subordinates = [];

    public function __construct(string $name, string $position)
    {
        $this->name = $name;
        $this->position = $position;
    }

    public function add(EmployeeInterface $employee): void
    {
        $this->subordinates[] = $employee;
    }

    public function remove(EmployeeInterface $employee): void
    {
        $this->subordinates = array_filter(
            $this->subordinates,
            static function ($e) use ($employee) {
                return $e !== $employee;
            }
        );
    }

    public function getDetails(): string
    {
        $details = $this->position . ": " . $this->name;
        foreach ($this->subordinates as $subordinate) {
            $details .= "\n  " . $subordinate->getDetails();
        }
        return $details;
    }
}