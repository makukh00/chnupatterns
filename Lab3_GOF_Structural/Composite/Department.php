<?php

namespace Composite;

class Department implements EmployeeInterface
{
    private string $name;
    private array $employees = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function add(EmployeeInterface $employee): void
    {
        $this->employees[] = $employee;
    }

    public function remove(EmployeeInterface $employee): void
    {
        $this->employees = array_filter(
            $this->employees,
            static function ($e) use ($employee) {
                return $e !== $employee;
            }
        );
    }

    public function getDetails(): string
    {
        $details = $this->name;
        foreach ($this->employees as $employee) {
            $details .= "\n  " . $employee->getDetails();
        }
        return $details;
    }
}