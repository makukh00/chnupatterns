<?php

namespace Composite;

interface EmployeeInterface
{
    public function getDetails(): string;

}